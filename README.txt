Description
===========
Allows the use of datedropper jQuery plugin to be used in your Drupal site.

Requirements
=================
jQuery 1.7+
elements module

Current functionality
=================

1. Global settings for all date fields, and ability to tag fields to use dates.
2. Allows elements to be created to use datedropper with form API.

Future functionality
=================

1. Full control of any field setting date type.
2. Customisable field elements per item.