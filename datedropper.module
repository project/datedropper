<?php
/**
 * @file
 * Adds datedropper to datefields as a widget.
 *
 * @TODO: add as a widget for date field types.
 * @TODO: add settings per field.
 * @TODO: add multingual support.
 */

/**
 * Implements hook_preprocess_page().
 */
function datedropper_preprocess_page(&$variables) {
  datedropper_load();
  drupal_add_js(drupal_get_path('module', 'datedropper') .
    '/datedropper-widget.js');
}

function datedropper_menu() {
  $items = array();
  if (module_exists('date')) {
    $path = 'date';
  }
  else {
    $path = 'user-interface';
  }
  $items['admin/config/' . $path . '/datedropper'] = array(
    'title' => 'Datedropper',
    'description' => 'Adjust Datedropper settings.',
    'file' => 'datedropper.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('datedropper_admin_settings'),
    'access arguments' => array('administer site configuration'),
  );
  return $items;
}

/**
 * Implements hook_library().
 */
function datedropper_library() {
  $libraries = array();
  $libraries['datedropper'] = array(
    'title' => 'Date dropper',
    'website' => 'http://felicegattuso.com/projects/datedropper/',
    'version' => '1.2',
    'js' => array(
      drupal_get_path('module', 'datedropper') . 'plugin/datedropper.min.js'
      => array(),
    ),
    'css' => array(
      drupal_get_path('module', 'datedropper') . '/plugin/datedropper.css'
      => array(
        'type' => 'file',
        'media' => 'screen',
      ),
    ),
  );
  return $libraries;
}

/**
 * Loads datedropper onto site based off the default settings.
 */
function datedropper_load() {
  static $datedropper_loaded;
  if ($datedropper_loaded) {
    return;
  }
  drupal_add_js(array('datedropper' =>
    array('elements' => variable_get('datedropper_elements'))), 'setting');

  $js_settings = array(
    'format' => variable_get('datedropper_format', 'd/M/Y'),
    'language' => variable_get('datedropper_lang', 'default'),
    'placeholder' => variable_get('datedropper_placeholder', t('Enter date')),
    'minYear' =>
      variable_get('datedropper_minyear', date("Y",strtotime("-1 year"))),
    'maxYear' =>
      variable_get('datedropper_maxyear', date("Y",strtotime("+1 year"))),
    'animate_current' => variable_get('datedropper_animation', 'dropdown'),
    'color' => variable_get('datedropper_mainhexcolour', '#f87a54'),
  );

  // @TODO: Add ability for settings to be overidden.
  drupal_add_js(array('dateDropper' => $js_settings),
    array('type' => 'setting', 'scope' => JS_DEFAULT));
  drupal_add_js(drupal_get_path('module', 'datedropper') .
    '/plugin/datedropper.min.js');
  drupal_add_css(drupal_get_path('module', 'datedropper') .
    '/plugin/datedropper.css');
}

/**
 * Implements hook_element_info().
 */
function datedropper_element_info() {
  $types['datedropper'] = array(
    '#input' => TRUE,
    '#size' => 20,
    '#maxlength' => 30,
    '#autocomplete_path' => FALSE,
    '#process' => array('ajax_process_form'),
    '#theme' => 'datedropper',
    '#theme_wrappers' => array('form_element'),
  );
  return $types;
}

/**
 * Implements hook_theme().
 */
function datedropper_theme() {
  return array(
    'datedropper' => array(
      'arguments' => array('element' => NULL),
      'render element' => 'element',
      'file' => 'datedropper.theme.inc',
    ),
  );
}