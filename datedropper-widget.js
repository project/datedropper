(function ($) {

    Drupal.behaviors.initDateDropper = {
        attach: function (context, settings) {
            var dd_elements = '.form-datedropper' + Drupal.settings.datedropper.elements;
            $(dd_elements, context).dateDropper(settings.dateDropper);
        }
    };

})(jQuery);