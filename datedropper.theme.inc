<?php

/**
 * @file
 * The theme include file for the datedropper module.
 *
 * Contains the theme functions datedropper FAPI elements.
 */

/**
 * Returns HTML for a datedropper form element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #title, #value, #description, #size, #maxlength,
 *     #placeholder, #required, #attributes.
 *
 * @ingroup themeable
 */
function theme_datedropper($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes($element,
    ('id', 'name', 'value', 'size', 'maxlength', 'placeholder'));
  _form_set_class($element,
    array('form-datedropper', 'form-text', 'form-control'));

  $extra = elements_add_autocomplete($element);
  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output . $extra;
}
