<?php

/**
 * @file
 * Administrative page callbacks for the datedropper module.
 */

/**
 * General configuration form for controlling the global datedropper behaviour.
 */
function datedropper_admin_settings() {
  $form = array();
  $library = libraries_detect('colorbox');
  if (!$library) {
    drupal_set_message(t('Datedropper is missing from libraries'), 'error');
  }

  $form['datedropper_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Date format'),
    '#description' => t('Set the default format for your date picker'),
    '#default_value' => variable_get('datedropper_format', 'd/M/Y'),
  );
  // @TODO: Remove $form['language'] and add based off global $lanugage.
  $form['datedropper_lang'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#options' => array(
      'ar' => t('Arabic'),
      'da' => t('Dansk'),
      'de' => t('Deutsch'),
      'nl' => t('Dutch'),
      'es' => t('Español'),
      'default' => t('English'),
      'fr' => t('Français'),
      'gr' => t('Greek'),
      'hu' => t('Hungarian'),
      'it' => t('Italian'),
      'pl' => t('Polish'),
      'pt' => t('Portugese'),
      'ru' => t('Russian'),
      'si' => t('Slovenian'),
      'uk' => t('Ukranian'),
      'tr' => t('Turkish'),
    ),
    '#default_value' => variable_get('datedropper_lang', 'default'),
  );
  $form['datedropper_placeholder'] = array(
    '#type' => 'textfield',
    '#title' => t('Placeholder'),
    '#default_value' => variable_get('datedropper_placeholder', t('Enter date')),
  );
  $form['datedropper_minyear'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum year'),
    '#default_value' => variable_get('datedropper_minyear', date("Y", strtotime("-1 year"))),
  );
  $form['datedropper_maxyear'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum year'),
    '#default_value' => variable_get('datedropper_maxyear', date("Y", strtotime("+1 year"))),
  );
  $form['datedropper_animation'] = array(
    '#type' => 'select',
    '#title' => t('Animation'),
    '#options' => array(
      'bounce' => t('Bounce'),
      'dropdown' => t('Dropdown'),
      'fadein' => t('Fadein'),
    ),
    '#default_value' => variable_get('datedropper_animation', 'dropdown'),
  );
  $form['datedropper_mainhexcolour'] = array(
    '#type' => 'textfield',
    '#title' => t('Main hex colour'),
    '#description' => t('default: "#f87a54"'),
    '#size' => 6,
    '#default_value' => variable_get('datedropper_mainhexcolour', '#f87a54'),
  );
  $form['datedropper_elements'] = array(
    '#type' => 'textarea',
    '#title' => t('Element id and classes'),
    '#default_value' => variable_get('datedropper_elements', '.date'),
  );
  return system_settings_form($form);
}